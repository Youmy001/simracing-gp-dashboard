Simracing-GP Dashboard
======================

This a simhub dashboard I use when I race on Simracing-GP servers in rFactor 2.

![](simracing-gp-dashboard.djson.01.png)

The dash includes six pages of information :
 1. Tyre temperatures
 2. Tyre wear
 3. Brake temperatures
 4. Lap times
 5. Drivers ahead and behind
 6. Proximity radar

Every pages show the delta to your current best lap as well as fuel and session information.

> You must assign the dashboard controls `Show next dash screen` and `Show previous dash screen` to buttons on your wheel, button box, controller, or keyboard to access all the pages while driving. 

Requirements
------------

 - Simhub version 7.4.23 or above
 - Open Sans Font Family ([Google Font](https://fonts.google.com/specimen/Open+Sans))
 - Play Font Family ([Google Font](https://fonts.google.com/specimen/Play?query=play))

Installation
------------

 1. Update Simhub to latest version
 2. Download the simhubdash from the [latest release](https://gitlab.com/Youmy001/simracing-gp-dashboard/-/releases)
 3. Double click on the simhubdash file. The dash will be imported into simhub.
